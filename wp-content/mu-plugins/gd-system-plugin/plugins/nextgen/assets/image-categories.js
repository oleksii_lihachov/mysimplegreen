import { __ } from '@wordpress/i18n';

export default [
	{
		name: __( 'Fashion', 'nextgen' ),
		slug: 'fashion',
		value: '',
	},
	{
		name: __( 'Home Decor', 'nextgen' ),
		slug: 'homedecor',
		value: '',
	},
	{
		name: __( 'Coffee', 'nextgen' ),
		slug: 'coffee',
		value: '',
	},
	{
		name: __( 'Construction', 'nextgen' ),
		slug: 'constructioncompany',
		value: '',
	},
	{
		name: __( 'Art', 'nextgen' ),
		slug: 'personal_art',
		value: '',
	},
	{
		name: __( 'Bakery', 'nextgen' ),
		slug: 'bakeries',
		value: '',
	},
	{
		name: __( 'Fitness', 'nextgen' ),
		slug: 'fitness',
		value: '',
	},
	{
		name: __( 'Landscaping', 'nextgen' ),
		slug: 'landscaping',
		value: '',
	},
];

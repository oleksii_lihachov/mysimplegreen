const path = require( 'path' );
const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );

module.exports = {
	...defaultConfig,

	entry: {
		'site-design': path.resolve( process.cwd(), 'assets/js/site-design', 'index.js' ),
		'site-design-editor': path.resolve( process.cwd(), 'assets/scss/site-design', 'admin-editor.scss' ),
		'site-content': path.resolve( process.cwd(), 'assets/js/site-content', 'index.js' ),
		'site-content-editor': path.resolve( process.cwd(), 'assets/scss/site-content', 'admin-editor.scss' ),
		'site-editor-behavior': path.resolve( process.cwd(), 'assets/js/site-editor', 'index.js' ),
		'block-editor': path.resolve( process.cwd(), 'assets/js', 'block-editor.js' ),
		'block-editor-style': path.resolve( process.cwd(), 'assets/scss', 'block-editor.scss' ),
		'publish-guide': path.resolve( process.cwd(), 'assets/js/publish-guide', 'index.js' ),
		'publish-guide-editor': path.resolve( process.cwd(), 'assets/scss/publish-guide', 'admin-editor.scss' ),
		'feedback-modal': path.resolve( process.cwd(), 'assets/js/feedback-modal', 'index.js' ),
		'feedback-modal-editor': path.resolve( process.cwd(), 'assets/scss/feedback-modal', 'admin-editor.scss' ),
		'nux-patterns': path.resolve( process.cwd(), 'assets/js/nux-patterns', 'index.js' ),
		'logo-menu': path.resolve( process.cwd(), 'assets/js/logo-menu', 'index.js' ),
		'logo-menu-editor': path.resolve( process.cwd(), 'assets/scss/logo-menu', 'editor.scss' ),
		'layout-selector': path.resolve( process.cwd(), 'assets/js/layout-selector', 'index.js' ),
		'layout-selector-editor': path.resolve( process.cwd(), 'assets/scss/layout-selector', 'editor.scss' ),
		'media-download': path.resolve( process.cwd(), 'assets/js/media-download', 'index.js' ),
	},

};

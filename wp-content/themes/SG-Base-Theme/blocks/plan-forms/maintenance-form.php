		<form method="post" novalidate enctype="multipart/form-data" action="https://www.formstack.com/forms/index.php" class="fsForm fsMultiColumn fsMaxCol4" id="fsForm4029231">
			<input type="hidden" name="form" value="4029231" />
			<input type="hidden" name="viewkey" value="qRR97MtlyP" />
			<input type="hidden" name="hidden_fields" id="hidden_fields4029231" value="" />
			<input type="hidden" name="_submit" value="1" />
			<input type="hidden" name="incomplete" id="incomplete4029231" value="" />
			<input type="hidden" name="incomplete_password" id="incomplete_password4029231" />
			<input type="hidden" name="style_version" value="3" />
			<input type="hidden" id="viewparam" name="viewparam" value="826720" />
			<div id="requiredFieldsError" style="display:none;">Please fill in a valid value for all required fields</div>
			<div id="invalidFormatError" style="display:none;">Please ensure all values are in a proper format.</div>
			<div id="resumeConfirm" style="display:none;">Are you sure you want to leave this form and resume later?</div>
			<div id="resumeConfirmPassword" style="display: none;">Are you sure you want to leave this form and resume later? If so, please enter a password below to securely save your form.</div>
			<div id="saveAndResume" style="display: none;">Save and Resume Later</div>
			<div id="saveResumeProcess" style="display: none;">Save and get link</div>
			<div id="fileTypeAlert" style="display:none;">You must upload one of the following file types for the selected field:</div>
			<div id="embedError" style="display:none;">There was an error displaying the form. Please copy and paste the embed code again.</div>
			<div id="applyDiscountButton" style="display:none;">Apply Discount</div>
			<div id="dcmYouSaved" style="display:none;">You saved</div>
			<div id="dcmWithCode" style="display:none;">with code</div>
			<div id="submitButtonText" style="display:none;">Submit</div>
			<div id="submittingText" style="display:none;">Submitting</div>
			<div id="validatingText" style="display:none;">Validating</div>
			<div id="autocaptureDisabledText" style="display:none;"></div>
			<div id="paymentInitError" style="display:none;">There was an error initializing the payment processor on this form. Please contact the form owner to correct this issue.</div>
			<div id="checkFieldPrompt" style="display:none;">Please check the field: </div>
			<div id="translatedWord-fields" style="display:none;">Fields</div>
			<div class="fsPage" id="fsPage4029231-1">
				<div id="ReactContainer4029231" style="display:none" class="FsReactContainerInitApp" data-fs-react-app-id="4029231"></div>
				<div class="contact" id="contact-form">
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97727001" lang="en" fs-field-type="text" fs-field-validation-name="First Name">
							<input
								type="text" id="field97727001"
								name="field97727001"
								placeholder="First Name*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97727002" lang="en" fs-field-type="text" fs-field-validation-name="Last Name">
							<input
								type="text" id="field97727002"
								name="field97727002"
								placeholder="Last Name*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97727004" lang="en" fs-field-type="email" fs-field-validation-name="E-mail Address">
							<input type="email" id="field97727004" name="field97727004" placeholder="Email*" required="required" value="" class="fsField fsFormatEmail fsRequired" aria-required="true" />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97727005" lang="en" fs-field-type="phone" fs-field-validation-name="Phone Number">
							<input type="tel" id="field97727005" name="field97727005" placeholder="Phone Number*" required value="" class="fsField fsFormatPhoneCA  fsRequired" aria-required="true" data-country="CA" data-format="user" />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97727006" lang="en" fs-field-type="text" fs-field-validation-name="Home Address">
							<input
								type="text" id="field97727006"
								name="field97727006"
								placeholder="Home Address*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97727003" lang="en" fs-field-type="text" fs-field-validation-name="Postal Code">
							<input
								type="text" id="field97727003"
								name="field97727003"
								placeholder="Postal Code*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97727277" lang="en" fs-field-type="text" fs-field-validation-name="Province">
							<input
								type="text" id="field97727277"
								name="field97727277"
								placeholder="Province*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97727397" lang="en" fs-field-type="text" fs-field-validation-name="Enbridge Account Number">
							<input
								type="text" id="field97727397"
								name="field97727397"
								placeholder="Enbridge Account Number*"
								value=""
								class="fsField fsFormatText    "
								/>
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100" id="fsCell97727409" lang="en" fs-field-type="radio" fs-field-validation-name="Select Your Plan">
							<fieldset role="group" aria-labelledby="fsLegend97727409" id="label97727409">
								<legend id="fsLegend97727409" class="fsLabel fsRequiredLabel fsLabelVertical"><span>Select Your Plan<span class="fsRequiredMarker">*</span></span></legend>
								<div class="fieldset-content">
									<label class="fsOptionLabel " for="field97727409_1"><input type="radio" id="field97727409_1" name="field97727409" value="Heating System Maintenance $10.95" class="fsField fsRequired " aria-required="true" /><span>Heating System Maintenance $10.95</span></label>
									<label class="fsOptionLabel " for="field97727409_2"><input type="radio" id="field97727409_2" name="field97727409" value="Cooling System Maintenance $10.95" class="fsField fsRequired " aria-required="true" /><span>Cooling System Maintenance $10.95</span></label>
									<label class="fsOptionLabel " for="field97727409_3"><input type="radio" id="field97727409_3" name="field97727409" value="Water Heating Maintenance $13.95" class="fsField fsRequired " aria-required="true" /><span>Water Heating Maintenance $13.95</span></label>
									<label class="fsOptionLabel " for="field97727409_4"><input type="radio" id="field97727409_4" name="field97727409" value="Additional Plan Inquiry" class="fsField fsRequired " aria-required="true" /><span>Additional Plan Inquiry</span></label>
								</div>
							</fieldset>
						</div>
					</div>
						<div class="legal-optin" id="fsCell98426501" lang="en" fs-field-type="checkbox" fs-field-validation-name="Terms & Conditions">
								<div class="fieldset-content">
									<label class="check-label" for="field98426501_1"><input type="checkbox" id="field98426501_1" name="field98426501[]" value="Agree" class="fsField vertical" /><span>* I agree to the <a href="https://www.mysimplygreen.com/wp-content/uploads/2020/10/Maintenance_Protection_Plan_Terms_and_Conditions.pdf" target="_blank" style="white-space:nowrap;">Terms & Conditions</a></span></label>
								</div>
						</div>
						<div class="legal-optin" id="fsCell98426580" lang="en" fs-field-type="checkbox" fs-field-validation-name="">
								<div class="fieldset-content">
									<label class="check-label" for="field98426580_1"><input type="checkbox" id="field98426580_1" name="field98426580[]" value="By providing your contact details you agree to receiving electronic communication from Simply Green Home Services and its subsidiaries. You may withdraw your consent any time." class="fsField vertical" /><span>* By providing your contact details you agree to receiving electronic communication from Simply Green Home Services and its subsidiaries. You may withdraw your consent any time.</span></label>
								</div>
						</div>

				<?php echo apply_filters( 'gglcptch_display_recaptcha', '', 'fstack_contact_form' ); ?>
				
				<?php $check_result = apply_filters( 'gglcptch_verify_recaptcha', true, 'string', 'fstack_contact_form' );
if ( true === $check_result ) { /* the reCAPTCHA answer is right */
/* do necessary action */
	echo '<div id="fsSubmit4029231" class="fsSubmit fsPagination">
				<button type="button" id="fsPreviousButton4029231" class="fsPreviousButton" value="Previous Page" style="display:none;" aria-label="Previous"><span class="fsFull">Previous</span><span class="fsSlim">&larr;</span></button>
				<button type="button" id="fsNextButton4029231" class="fsNextButton" value="Next Page" style="display:none;" aria-label="Next"><span class="fsFull">Next</span><span class="fsSlim">&rarr;</span></button>
				<input id="fsSubmitButton4029231"
					class="fsSubmitButton"
					style="display: block;"
					type="submit"
					value="Submit" />
				<div class="clear"></div>
				<div class="withAds"></div>
			</div>';
} else { /* the reCAPTCHA answer is wrong or there are some other errors */
echo $check_result; /* display the error message or do other necessary actions in case when the reCAPTCHA test was failed */
} ?>

				
			
			<script type="text/javascript">
				window.FS_FIELD_DATA_4029231 = [];
			</script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/jquery.min.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/jquery-ui.min.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/scripts.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/analytics.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/google-phone-lib.js" ></script>
			<script type="text/javascript">
				(function() {
				if (typeof sessionStorage !== 'undefined' && sessionStorage.fsFonts) {
				document.documentElement.className = document.documentElement.className += ' wf-active';
				}
				var pre = document.createElement('link');
				pre.rel  = 'preconnect';
				pre.href = 'https://fonts.googleapis.com/';
				pre.setAttribute('crossorigin', '');
				var s = document.getElementsByTagName('head')[0];
				s.appendChild(pre);
				var fontConfig = {
				google: {
				families: [
				'Muli:400,300,700'
				]
				},
				timeout: 2000,
				active: function() {
				if (typeof sessionStorage === 'undefined') {
				return;
				}
				sessionStorage.fsFonts = true;
				}
				};
				if (typeof WebFont === 'undefined') {
				window.WebFontConfig = fontConfig;
				var wf = document.createElement('script');
				wf.type  = 'text/javascript';
				wf.async = 'true';
				wf.src   = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				s.appendChild(wf);
				} else {
				WebFont.load(fontConfig);
				}
				})();
				if(window.addEventListener) {
				window.addEventListener('load', loadFormstack, false);
				} else if(window.attachEvent) {
				window.attachEvent('onload', loadFormstack);
				} else {
				loadFormstack();
				}
				function loadFormstack() {
				var form4029231 = new Formstack.Form(4029231, 'https://www.formstack.com/forms/');
				form4029231.logicFields = [];
				form4029231.calcFields = [];
				form4029231.dateCalcFields = [];
				form4029231.init();
				if (Formstack.Analytics) {
				form4029231.plugins.analytics = new Formstack.Analytics('https://www.formstack.com', 4029231, form4029231);
				form4029231.plugins.analytics.trackTouch();
				form4029231.plugins.analytics.trackBottleneck();
				}
				window.form4029231 = form4029231;
				};
			</script></div>
		</form>
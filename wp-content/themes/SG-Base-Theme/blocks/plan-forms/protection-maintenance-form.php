		<form method="post" novalidate enctype="multipart/form-data" action="https://www.formstack.com/forms/index.php" class="fsForm fsMultiColumn fsMaxCol4" id="fsForm4029275">
			<input type="hidden" name="form" value="4029275" />
			<input type="hidden" name="viewkey" value="qRR97MtlyP" />
			<input type="hidden" name="hidden_fields" id="hidden_fields4029275" value="" />
			<input type="hidden" name="_submit" value="1" />
			<input type="hidden" name="incomplete" id="incomplete4029275" value="" />
			<input type="hidden" name="incomplete_password" id="incomplete_password4029275" />
			<input type="hidden" name="style_version" value="3" />
			<input type="hidden" id="viewparam" name="viewparam" value="826720" />
			<div id="requiredFieldsError" style="display:none;">Please fill in a valid value for all required fields</div>
			<div id="invalidFormatError" style="display:none;">Please ensure all values are in a proper format.</div>
			<div id="resumeConfirm" style="display:none;">Are you sure you want to leave this form and resume later?</div>
			<div id="resumeConfirmPassword" style="display: none;">Are you sure you want to leave this form and resume later? If so, please enter a password below to securely save your form.</div>
			<div id="saveAndResume" style="display: none;">Save and Resume Later</div>
			<div id="saveResumeProcess" style="display: none;">Save and get link</div>
			<div id="fileTypeAlert" style="display:none;">You must upload one of the following file types for the selected field:</div>
			<div id="embedError" style="display:none;">There was an error displaying the form. Please copy and paste the embed code again.</div>
			<div id="applyDiscountButton" style="display:none;">Apply Discount</div>
			<div id="dcmYouSaved" style="display:none;">You saved</div>
			<div id="dcmWithCode" style="display:none;">with code</div>
			<div id="submitButtonText" style="display:none;">Submit</div>
			<div id="submittingText" style="display:none;">Submitting</div>
			<div id="validatingText" style="display:none;">Validating</div>
			<div id="autocaptureDisabledText" style="display:none;"></div>
			<div id="paymentInitError" style="display:none;">There was an error initializing the payment processor on this form. Please contact the form owner to correct this issue.</div>
			<div id="checkFieldPrompt" style="display:none;">Please check the field: </div>
			<div id="translatedWord-fields" style="display:none;">Fields</div>
			<div class="fsPage" id="fsPage4029275-1">
				<div id="ReactContainer4029275" style="display:none" class="FsReactContainerInitApp" data-fs-react-app-id="4029275"></div>
				<div class="contact" id="contact-form">
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97728058" lang="en" fs-field-type="text" fs-field-validation-name="First Name">
							<input
								type="text" id="field97728058"
								name="field97728058"
								placeholder="First Name*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97728059" lang="en" fs-field-type="text" fs-field-validation-name="Last Name">
							<input
								type="text" id="field97728059"
								name="field97728059"
								placeholder="Last Name*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97728060" lang="en" fs-field-type="email" fs-field-validation-name="E-mail Address">
							<input type="email" id="field97728060" name="field97728060" placeholder="Email*" required="required" value="" class="fsField fsFormatEmail fsRequired" aria-required="true" />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97728061" lang="en" fs-field-type="phone" fs-field-validation-name="Phone Number">
							<input type="tel" id="field97728061" name="field97728061" placeholder="Phone Number*" required value="" class="fsField fsFormatPhoneCA  fsRequired" aria-required="true" data-country="CA" data-format="user" />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97728062" lang="en" fs-field-type="text" fs-field-validation-name="Home Address">
							<input
								type="text" id="field97728062"
								name="field97728062"
								placeholder="Home Address*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97728063" lang="en" fs-field-type="text" fs-field-validation-name="Postal Code">
							<input
								type="text" id="field97728063"
								name="field97728063"
								placeholder="Postal Code*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLabelVertical fsSpan50" id="fsCell97728064" lang="en" fs-field-type="text" fs-field-validation-name="Province">
							<input
								type="text" id="field97728064"
								name="field97728064"
								placeholder="Province*"
								required       value=""
								class="fsField fsFormatText fsRequired   "
								aria-required="true"     />
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsLast fsLabelVertical fsSpan50" id="fsCell97728065" lang="en" fs-field-type="text" fs-field-validation-name="Enbridge Account Number">
							<input
								type="text" id="field97728065"
								name="field97728065"
								placeholder="Enbridge Account Number*"
								value=""
								class="fsField fsFormatText    "
								/>
						</div>
						<div class="fsRowBody fsCell fsFieldCell fsFirst fsLast fsLabelVertical fsSpan100" id="fsCell97728066" lang="en" fs-field-type="radio" fs-field-validation-name="Select Your Plan">
							<fieldset role="group" aria-labelledby="fsLegend97728066" id="label97728066">
								<legend id="fsLegend97728066" class="fsLabel fsRequiredLabel fsLabelVertical"><span>Select Your Plan<span class="fsRequiredMarker">*</span></span></legend>
								<div class="fieldset-content">
									<label class="fsOptionLabel " for="field97728066_1"><input type="radio" id="field97728066_1" name="field97728066" value="Heating System Maintenance &amp; Protection $23.95" class="fsField fsRequired " aria-required="true" /><span>Heating System Maintenance &amp; Protection $23.95</span></label>
									<label class="fsOptionLabel " for="field97728066_2"><input type="radio" id="field97728066_2" name="field97728066" value="Cooling System Maintenance &amp; Protection $23.95" class="fsField fsRequired " aria-required="true" /><span>Cooling System Maintenance &amp; Protection $23.95</span></label>
									<label class="fsOptionLabel " for="field97728066_3"><input type="radio" id="field97728066_3" name="field97728066" value="Water Heating Maintenance &amp; Protection  $27.95" class="fsField fsRequired " aria-required="true" /><span>Water Heating Maintenance &amp; Protection  $27.95</span></label>
									<label class="fsOptionLabel " for="field97728066_4"><input type="radio" id="field97728066_4" name="field97728066" value="Heating &amp; Cooling Systems Maintenance &amp; Protection  $35.95" class="fsField fsRequired " aria-required="true" /><span>Heating &amp; Cooling Systems Maintenance &amp; Protection  $35.95</span></label>
									<label class="fsOptionLabel " for="field97728066_5"><input type="radio" id="field97728066_5" name="field97728066" value="Heating &amp; Cooling Systems + Water Heating Maintenance &amp; Protection $45.95" class="fsField fsRequired " aria-required="true" /><span>Heating &amp; Cooling Systems + Water Heating Maintenance &amp; Protection $45.95</span></label>
									<label class="fsOptionLabel " for="field97728066_6"><input type="radio" id="field97728066_6" name="field97728066" value="Additional Plan Inquiry" class="fsField fsRequired " aria-required="true" /><span>Additional Plan Inquiry</span></label>
								</div>
							</fieldset>
						</div></div>
						<div class="legal-optin" id="fsCell98426822" lang="en" fs-field-type="checkbox" fs-field-validation-name="Terms & Conditions">
								<div class="fieldset-content">
									<label class="check-label" for="field98426822_1"><input type="checkbox" id="field98426822_1" name="field98426822[]" value="Agree" class="fsField vertical" /><span>* I agree to the <a href="https://www.mysimplygreen.com/wp-content/uploads/2020/10/Maintenance_Protection_Plan_Terms_and_Conditions.pdf" target="_blank" style="white-space:nowrap;">Terms & Conditions</a></span></label>
								</div>
						</div>
						<div class="legal-optin" id="fsCell98426908" lang="en" fs-field-type="checkbox" fs-field-validation-name="">
								<div class="fieldset-content">
									<label class="check-label" for="field98426908_1"><input type="checkbox" id="field98426908_1" name="field98426908[]" value="By providing your contact details you agree to receiving electronic communication from Simply Green Home Services and its subsidiaries. You may withdraw your consent any time." class="fsField vertical" /><span>* By providing your contact details you agree to receiving electronic communication from Simply Green Home Services and its subsidiaries. You may withdraw your consent any time.</span></label>
								</div>
						</div>

				<?php echo apply_filters( 'gglcptch_display_recaptcha', '', 'fstack_contact_form' ); ?>
				
				<?php $check_result = apply_filters( 'gglcptch_verify_recaptcha', true, 'string', 'fstack_contact_form' );
if ( true === $check_result ) { /* the reCAPTCHA answer is right */
/* do necessary action */
	echo '<div id="fsSubmit4029275" class="fsSubmit fsPagination">
				<button type="button" id="fsPreviousButton4029275" class="fsPreviousButton" value="Previous Page" style="display:none;" aria-label="Previous"><span class="fsFull">Previous</span><span class="fsSlim">&larr;</span></button>
				<button type="button" id="fsNextButton4029275" class="fsNextButton" value="Next Page" style="display:none;" aria-label="Next"><span class="fsFull">Next</span><span class="fsSlim">&rarr;</span></button>
				<input id="fsSubmitButton4029275"
					class="fsSubmitButton"
					style="display: block;"
					type="submit"
					value="Submit" />
				<div class="clear"></div>
				<div class="withAds"></div>
			</div>';
} else { /* the reCAPTCHA answer is wrong or there are some other errors */
echo $check_result; /* display the error message or do other necessary actions in case when the reCAPTCHA test was failed */
} ?>

				
			
			<script type="text/javascript">
				window.FS_FIELD_DATA_4029275 = [];
			</script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/jquery.min.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/jquery-ui.min.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/scripts.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/analytics.js" ></script>
			<script type="text/javascript" src="//static.formstack.com/forms/js/3/google-phone-lib.js" ></script>
			<script type="text/javascript">
				(function() {
				if (typeof sessionStorage !== 'undefined' && sessionStorage.fsFonts) {
				document.documentElement.className = document.documentElement.className += ' wf-active';
				}
				var pre = document.createElement('link');
				pre.rel  = 'preconnect';
				pre.href = 'https://fonts.googleapis.com/';
				pre.setAttribute('crossorigin', '');
				var s = document.getElementsByTagName('head')[0];
				s.appendChild(pre);
				var fontConfig = {
				google: {
				families: [
				'Muli:400,300,700'
				]
				},
				timeout: 2000,
				active: function() {
				if (typeof sessionStorage === 'undefined') {
				return;
				}
				sessionStorage.fsFonts = true;
				}
				};
				if (typeof WebFont === 'undefined') {
				window.WebFontConfig = fontConfig;
				var wf = document.createElement('script');
				wf.type  = 'text/javascript';
				wf.async = 'true';
				wf.src   = ('https:' == document.location.protocol ? 'https' : 'http') +
				'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				s.appendChild(wf);
				} else {
				WebFont.load(fontConfig);
				}
				})();
				if(window.addEventListener) {
				window.addEventListener('load', loadFormstack, false);
				} else if(window.attachEvent) {
				window.attachEvent('onload', loadFormstack);
				} else {
				loadFormstack();
				}
				function loadFormstack() {
				var form4029275 = new Formstack.Form(4029275, 'https://www.formstack.com/forms/');
				form4029275.logicFields = [];
				form4029275.calcFields = [];
				form4029275.dateCalcFields = [];
				form4029275.init();
				if (Formstack.Analytics) {
				form4029275.plugins.analytics = new Formstack.Analytics('https://www.formstack.com', 4029275, form4029275);
				form4029275.plugins.analytics.trackTouch();
				form4029275.plugins.analytics.trackBottleneck();
				}
				window.form4029275 = form4029275;
				};
			</script></div>
		</form>

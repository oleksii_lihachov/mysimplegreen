<section class="partner-block">
	<div class="inner-box">
		<div class="image-side"><img src="<?php block_field('partner-logo');?>"></div>
		<div class="text-side">
			<h2><?php block_field('title');?></h2>
			<p style="font-weight:bold"><?php block_field('subheader');?></p>
			<?php block_field('copy');?>
		</div>
	</div>
</section>
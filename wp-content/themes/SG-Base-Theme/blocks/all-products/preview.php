<section class="category-list  <?php sps_category(); ?>">
    <div class="inner-box">
        <h2>Related Products</h2>
        <nav id="tab-block">
            <ul>
                <li id="heating">Heating</li>
                <li id="cooling">Cooling</li>
                <li id="air-filtration">Air Filtration</li>
                <li id="water-heating">Water Heating</li>
                <li id="water-treatment">Water Treatment</li>
            </ul>
        </nav>
        <div class="product-grid">
<h3 style="text-align:center;">The complete product list goes here. Omitted in the preview for brevity.</h3>
</div>
</div>
</section>

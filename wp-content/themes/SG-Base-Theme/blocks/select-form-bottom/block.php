<section class="select-form-bottom">
	<div class="inner-box">
		<h2><?php block_field('title');?></h2>
		<?php block_field('text');?>
		<div class="fsBody" id="fsLocal">
			<div class="fsBottomPage">
				<?php include block_value('available-forms');?>
			</div>
		</div>
	</div>
</section>
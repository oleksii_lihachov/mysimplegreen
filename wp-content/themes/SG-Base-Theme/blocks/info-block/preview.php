<section class="info-block <?php block_field('background-color'); ?>" >
	<div class="inner-box">
		<h2><?php block_field('title');?></h2>
		<?php block_field('copy'); ?>
		<?php block_field('custom'); ?>
	</div>
</section>
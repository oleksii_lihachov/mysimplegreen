<section class="big-dot" style="background-image:url(<?php block_field('background');?>);">
	<div class="dot-wrapper">
		<div class="text-side">
			<div class="copy"><span>
				<h2><?php block_field('title');?></h2>
				<?php block_field('copy-block'); ?></span>
			</div>
		</div>
	</div>
</section>
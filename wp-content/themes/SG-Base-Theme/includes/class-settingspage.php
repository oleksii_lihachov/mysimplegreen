<?php
/**
 * Settings Page
 *
 * @package null
 */

/**
 * Class
 */
class SettingsPage {
	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}

	/**
	 * Admin menu
	 *
	 * @return void
	 */
	public function admin_menu() {
		add_options_page(
			'Site Settings',
			'Site Settings',
			'manage_options',
			'site-options',
			array( $this, 'settings_page' )
		);
	}

	/**
	 * Register settings
	 *
	 * @return void
	 */
	public function register_settings() {
		// Google reCAPTCHA keys.
		add_settings_section(
			'gr_keys_section', // section ID.
			'Google reCAPTCHA Keys', // title (if needed).
			'', // callback function (if needed).
			'site-options' // page slug.
		);

		add_settings_field(
			'gr_key',
			'Key',
			array( $this, 'gr_key_html' ), // function which prints the field.
			'site-options', // page slug.
			'gr_keys_section', // section ID.
			array(
				'label_for' => 'gr_key',
			)
		);

		register_setting(
			'site-options', // settings group name.
			'gr_key', // option name.
			'sanitize_text_field' // sanitization function.
		);

		add_settings_field(
			'gr_secret_key',
			'Secret Key',
			array( $this, 'gr_secret_key_html' ), // function which prints the field.
			'site-options', // page slug.
			'gr_keys_section', // section ID.
			array(
				'label_for' => 'gr_secret_key',
			)
		);

		register_setting(
			'site-options', // settings group name.
			'gr_secret_key', // option name.
			'sanitize_text_field' // sanitization function.
		);
	}

	/**
	 * Google reCAPTCHA key
	 *
	 * @return void
	 */
	public function gr_key_html() {
		$key = get_option( 'gr_key' );

		printf(
			'<input type="text" id="gr_key" name="gr_key" value="%s" />',
			esc_attr( $key )
		);
	}

	/**
	 * Google reCAPTCHA secret key
	 *
	 * @return void
	 */
	public function gr_secret_key_html() {
		$key = get_option( 'gr_secret_key' );

		printf(
			'<input type="text" id="gr_secret_key" name="gr_secret_key" value="%s" />',
			esc_attr( $key )
		);
	}

	/**
	 * Page html
	 *
	 * @return void
	 */
	public function settings_page() {
		?>
		<div class="wrap">
			<h1>Site Settings</h1>
			<hr/>
			<form method="post" action="options.php">
				<?php
				settings_fields( 'site-options' );
				do_settings_sections( 'site-options' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}
}

new SettingsPage();

<?php
/**
 * Google reCAPTCHA implementations.
 *
 * @package null
 */

/**
 * Class.
 */
class ReCAPTCHA {
	/**
	 * Key
	 *
	 * @var mixed
	 */
	private $gr_key;

	/**
	 * Secret key
	 *
	 * @var mixed
	 */
	private $gr_secret_key;

	/**
	 * Verify url
	 * https://developers.google.com/recaptcha/docs/verify
	 *
	 * @var string
	 */
	private $gr_verify_url = 'https://www.google.com/recaptcha/api/siteverify';

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {
		$gr_key        = get_option( 'gr_key' );
		$gr_secret_key = get_option( 'gr_secret_key' );

		if ( ! $gr_key || ! $gr_secret_key ) {
			return;
		}

		$this->gr_key        = $gr_key;
		$this->gr_secret_key = $gr_secret_key;

		add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts' ) );
		add_action( 'wp_ajax_token_handler', array( $this, 'token_handler_callback' ) );
		add_action( 'wp_ajax_nopriv_token_handler', array( $this, 'token_handler_callback' ) );
	}

	/**
	 * Scripts
	 *
	 * @return void
	 */
	public function add_scripts() {
		wp_register_script(
			'g-recaptcha-api',
			'https://www.google.com/recaptcha/api.js?render=' . $this->gr_key,
			null,
			'1',
			true
		);

		wp_register_script(
			'g-recaptcha-frontend',
			get_template_directory_uri() . '/js/google-recaptcha-frontend.js',
			array( 'jquery' ),
			'15',
			true
		);

		wp_enqueue_script( 'g-recaptcha-api' );
		wp_enqueue_script( 'g-recaptcha-frontend' );

		wp_localize_script(
			'g-recaptcha-frontend',
			'grKeys',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'site_key' => $this->gr_key,
			)
		);
	}

	/**
	 * Retrieve google token and test result
	 *
	 * @return void
	 */
	public function token_handler_callback() {
		$token = $_GET['token'];

		if ( empty( $token ) ) {
			wp_send_json_error(
				array(
					'message' => 'Not found Google reCAPTCHA token',
				),
				400
			);
		}

		$post_data = array(
			'secret'   => $this->gr_secret_key,
			'response' => $token,
		);

		$ch = curl_init( $this->gr_verify_url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$curl_response = curl_exec( $ch );
		$response      = json_decode( $curl_response );
		curl_close( $ch );

		// Transform to assoc array.
		$data = (array) $response;

		if ( false === $data['success'] ) {
			wp_send_json_error(
				array(
					'message' => $data['error-codes'][0],
				),
				400
			);
		} else {
			wp_send_json_success( $data );
		}
	}
}
new ReCAPTCHA();

(function ($) {
    $(document).ready(function () {
        var $submitBtns = $('[data-grecaptcha]');
        if ($submitBtns.length) {
            $submitBtns.each(function () {
                $(this).on('click', function (event) {
                    event.preventDefault();

                    var $button = $(this);

                    grecaptcha.ready(function () {
                        grecaptcha.execute(
                            window.grKeys.site_key,
                            { action: 'submit' }).then(function (token) {
                                if (!token) {
                                    console.error('No token from Google reCAPTCHA');
                                }

                                $.ajax({
                                    method: 'GET',
                                    url: window.grKeys.ajax_url,
                                    data: {
                                        token: token,
                                        action: 'token_handler'
                                    },
                                    success: function (response) {
                                        if (response.success && response.data.success) {
                                            var $form = $button.closest('form');
                                            $form.find('.fsError').hide();
                                            $form.submit();
                                        }
                                    },
                                    error: function (response) {
                                        const output = JSON.parse(response.responseText);
                                        console.error(output);
                                        $button.closest('form').append('<div class="fsError">' + output.data.message + '</div>');
                                    }
                                });
                            });
                    });
                });
            });
        }
    });
}(jQuery));